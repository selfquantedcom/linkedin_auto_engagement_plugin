
try{
   chrome.storage.sync.get('itemHashtag', function(data) {
      var itemHashtag = data.itemHashtag;
      console.log(itemHashtag);
      if(itemHashtag){         
         clearInterval(window.likeSharedItems);         
         window.likeSharedItems = setInterval(function(){  
            likeSharedItemsRelatedTo(itemHashtag);
         }, 5000);      
      }
      
   })
   // Load default value
} catch {
   chrome.storage.sync.set({itemHashtag: ''})
}

// helper functions
function likeSharedItemsRelatedTo(itemHashtag){   
   var allDisplayedItems = document.querySelectorAll('.relative .ember-view');
   for (i = 0; i < allDisplayedItems.length; i++) {   
      var sharedArticle = allDisplayedItems[i].querySelector('.feed-shared-text__text-view');
      if(sharedArticle && sharedArticle.innerText.includes(itemHashtag)){
         var likeButton = allDisplayedItems[i].querySelector('.reactions-react-button button');
         if (likeButton && likeButton.getAttribute('aria-pressed') == 'false'){                  
            likeButton.click();            
         }
      }
   }   
}
