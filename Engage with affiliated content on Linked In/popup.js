
'use strict';

// Prefil value for the user
chrome.storage.sync.get('itemHashtag', function(data) {
   if(data.itemHashtag){
      document.getElementById('itemHashtag').value = data.itemHashtag;      
   }
});

let form = document.getElementById('setItemHashtag');

form.onsubmit = function(event) {   
   event.preventDefault()
   var itemHashtag = event.target.children[0].value;
   chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
      chrome.tabs.executeScript(
         tabs[0].id,
         {code: `          
            function likeSharedItemsRelatedTo(itemHashtag){
               var allDisplayedItems = document.querySelectorAll('.relative .ember-view');
               for (i = 0; i < allDisplayedItems.length; i++) {   
                  var sharedArticle = allDisplayedItems[i].querySelector('.feed-shared-text__text-view');
                  if(sharedArticle && sharedArticle.innerText.includes(itemHashtag)){
                     var likeButton = allDisplayedItems[i].querySelector('.reactions-react-button button');
                     if (likeButton && likeButton.getAttribute('aria-pressed') == 'false'){                             
                        likeButton.click()            
                     }
                  }
               }   
            }  
            console.log("${itemHashtag}");            
            clearInterval(window.likeSharedItems);
            
            if("${itemHashtag}"){
               window.likeSharedItems = setInterval(function(){  
                  likeSharedItemsRelatedTo("${itemHashtag}");
               }, 5000);
            }
            chrome.storage.sync.set({itemHashtag: "${itemHashtag}"});
            `
         });
   });
};
